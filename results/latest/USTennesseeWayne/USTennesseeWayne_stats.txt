43 Divergences 
Computed from 120 by 121 log-likelihood matrix

         Estimate       SE
elpd_loo  -316.98    63.10
p_loo      345.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       90   74.4%
 (0.5, 0.7]   (ok)          3    2.5%
   (0.7, 1]   (bad)        19   15.7%
   (1, Inf)   (very bad)    9    7.4%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
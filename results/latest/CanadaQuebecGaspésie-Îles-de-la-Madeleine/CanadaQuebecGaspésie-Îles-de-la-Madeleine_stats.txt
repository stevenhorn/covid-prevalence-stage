54 Divergences 
Computed from 600 by 121 log-likelihood matrix

         Estimate       SE
elpd_loo  -370.76    41.73
p_loo      139.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      111   91.7%
 (0.5, 0.7]   (ok)          7    5.8%
   (0.7, 1]   (bad)         2    1.7%
   (1, Inf)   (very bad)    1    0.8%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
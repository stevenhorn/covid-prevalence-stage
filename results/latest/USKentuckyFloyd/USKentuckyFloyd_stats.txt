40 Divergences 
Computed from 160 by 124 log-likelihood matrix

         Estimate       SE
elpd_loo  -225.68    35.56
p_loo      145.36        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      101   81.5%
 (0.5, 0.7]   (ok)          5    4.0%
   (0.7, 1]   (bad)         6    4.8%
   (1, Inf)   (very bad)   12    9.7%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
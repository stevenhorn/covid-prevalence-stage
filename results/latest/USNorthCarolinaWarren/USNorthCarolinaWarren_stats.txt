0 Divergences 
Computed from 120 by 121 log-likelihood matrix

         Estimate       SE
elpd_loo  -219.52    17.51
p_loo        9.93        -
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      119   98.3%
 (0.5, 0.7]   (ok)          2    1.7%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    0    0.0%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
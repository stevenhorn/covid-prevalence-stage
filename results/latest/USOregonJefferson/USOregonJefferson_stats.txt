2 Divergences 
Computed from 160 by 124 log-likelihood matrix

         Estimate       SE
elpd_loo  -224.77    20.16
p_loo       31.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      118   95.2%
 (0.5, 0.7]   (ok)          2    1.6%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    4    3.2%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
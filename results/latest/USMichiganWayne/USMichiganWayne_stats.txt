75 Divergences 
Computed from 160 by 124 log-likelihood matrix

         Estimate       SE
elpd_loo  -838.18    14.54
p_loo       67.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       93   75.0%
 (0.5, 0.7]   (ok)         11    8.9%
   (0.7, 1]   (bad)         8    6.5%
   (1, Inf)   (very bad)   12    9.7%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
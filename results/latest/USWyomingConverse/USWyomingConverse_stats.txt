22 Divergences 
Computed from 160 by 123 log-likelihood matrix

         Estimate       SE
elpd_loo -1096.63   311.26
p_loo     2764.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)        3    2.4%
 (0.5, 0.7]   (ok)          1    0.8%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)  119   96.7%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
39 Divergences 
Computed from 120 by 121 log-likelihood matrix

         Estimate       SE
elpd_loo -16135.10  3421.27
p_loo    38205.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)        2    1.7%
 (0.5, 0.7]   (ok)          5    4.1%
   (0.7, 1]   (bad)        12    9.9%
   (1, Inf)   (very bad)  102   84.3%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
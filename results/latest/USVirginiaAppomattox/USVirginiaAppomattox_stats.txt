50 Divergences 
Computed from 160 by 123 log-likelihood matrix

         Estimate       SE
elpd_loo  -189.08    30.26
p_loo       96.66        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      103   83.7%
 (0.5, 0.7]   (ok)          4    3.3%
   (0.7, 1]   (bad)         1    0.8%
   (1, Inf)   (very bad)   15   12.2%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
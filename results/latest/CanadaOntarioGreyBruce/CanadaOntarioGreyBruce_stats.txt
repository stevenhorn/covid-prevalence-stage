115 Divergences 
Computed from 600 by 121 log-likelihood matrix

         Estimate       SE
elpd_loo  -163.28    14.09
p_loo        7.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      107   88.4%
 (0.5, 0.7]   (ok)          5    4.1%
   (0.7, 1]   (bad)         1    0.8%
   (1, Inf)   (very bad)    8    6.6%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
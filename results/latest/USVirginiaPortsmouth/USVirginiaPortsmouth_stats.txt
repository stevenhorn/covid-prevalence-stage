1 Divergences 
Computed from 160 by 123 log-likelihood matrix

         Estimate       SE
elpd_loo  -359.73    13.34
p_loo       15.87        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      109   88.6%
 (0.5, 0.7]   (ok)          8    6.5%
   (0.7, 1]   (bad)         4    3.3%
   (1, Inf)   (very bad)    2    1.6%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
140 Divergences 
Computed from 160 by 126 log-likelihood matrix

         Estimate       SE
elpd_loo -11213.54  3393.30
p_loo    36576.68        -
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      125   99.2%
 (0.5, 0.7]   (ok)          1    0.8%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    0    0.0%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
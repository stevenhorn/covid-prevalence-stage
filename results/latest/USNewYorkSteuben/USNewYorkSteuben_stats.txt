16 Divergences 
Computed from 120 by 121 log-likelihood matrix

         Estimate       SE
elpd_loo  -287.25    24.10
p_loo       41.51        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      110   90.9%
 (0.5, 0.7]   (ok)          9    7.4%
   (0.7, 1]   (bad)         1    0.8%
   (1, Inf)   (very bad)    1    0.8%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
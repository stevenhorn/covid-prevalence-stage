50 Divergences 
Computed from 120 by 121 log-likelihood matrix

         Estimate       SE
elpd_loo  -227.87    38.99
p_loo      197.01        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       21   17.4%
 (0.5, 0.7]   (ok)         81   66.9%
   (0.7, 1]   (bad)        17   14.0%
   (1, Inf)   (very bad)    2    1.7%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
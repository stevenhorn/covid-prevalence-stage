10 Divergences 
Computed from 160 by 123 log-likelihood matrix

         Estimate       SE
elpd_loo  -464.18    13.58
p_loo       17.36        -
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      118   95.9%
 (0.5, 0.7]   (ok)          5    4.1%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    0    0.0%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
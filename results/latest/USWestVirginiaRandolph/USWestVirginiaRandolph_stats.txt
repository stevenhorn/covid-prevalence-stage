7 Divergences 
Computed from 160 by 123 log-likelihood matrix

         Estimate       SE
elpd_loo  -211.33    30.63
p_loo       19.22        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      111   90.2%
 (0.5, 0.7]   (ok)         11    8.9%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    1    0.8%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
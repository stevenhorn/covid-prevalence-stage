29 Divergences 
Computed from 160 by 123 log-likelihood matrix

         Estimate       SE
elpd_loo  -293.06    39.01
p_loo      202.75        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       91   74.0%
 (0.5, 0.7]   (ok)          5    4.1%
   (0.7, 1]   (bad)         6    4.9%
   (1, Inf)   (very bad)   21   17.1%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
46 Divergences 
Computed from 120 by 121 log-likelihood matrix

         Estimate       SE
elpd_loo -1134.04   243.47
p_loo     2468.74        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      117   96.7%
 (0.5, 0.7]   (ok)          0    0.0%
   (0.7, 1]   (bad)         2    1.7%
   (1, Inf)   (very bad)    2    1.7%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
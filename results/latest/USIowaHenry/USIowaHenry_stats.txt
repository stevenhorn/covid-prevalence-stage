19 Divergences 
Computed from 160 by 124 log-likelihood matrix

         Estimate       SE
elpd_loo  -275.02    24.25
p_loo       91.15        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       77   62.1%
 (0.5, 0.7]   (ok)          4    3.2%
   (0.7, 1]   (bad)        10    8.1%
   (1, Inf)   (very bad)   33   26.6%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
47 Divergences 
Computed from 120 by 121 log-likelihood matrix

         Estimate       SE
elpd_loo  -109.93    13.39
p_loo        8.78        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      108   89.3%
 (0.5, 0.7]   (ok)          3    2.5%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)   10    8.3%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
26 Divergences 
Computed from 160 by 124 log-likelihood matrix

         Estimate       SE
elpd_loo -19492.43  3987.44
p_loo    40913.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       38   30.6%
 (0.5, 0.7]   (ok)         84   67.7%
   (0.7, 1]   (bad)         1    0.8%
   (1, Inf)   (very bad)    1    0.8%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
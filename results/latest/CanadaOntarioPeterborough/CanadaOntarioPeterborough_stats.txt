187 Divergences 
Computed from 600 by 121 log-likelihood matrix

         Estimate       SE
elpd_loo  -241.99    35.74
p_loo       86.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       83   68.6%
 (0.5, 0.7]   (ok)         13   10.7%
   (0.7, 1]   (bad)        14   11.6%
   (1, Inf)   (very bad)   11    9.1%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
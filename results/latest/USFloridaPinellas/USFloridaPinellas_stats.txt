0 Divergences 
Computed from 160 by 124 log-likelihood matrix

         Estimate       SE
elpd_loo  -549.89    18.76
p_loo       31.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      120   96.8%
 (0.5, 0.7]   (ok)          1    0.8%
   (0.7, 1]   (bad)         1    0.8%
   (1, Inf)   (very bad)    2    1.6%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
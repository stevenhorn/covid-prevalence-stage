81 Divergences 
Computed from 120 by 121 log-likelihood matrix

         Estimate       SE
elpd_loo  -474.01   282.47
p_loo     1042.58        -
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      104   86.0%
 (0.5, 0.7]   (ok)         17   14.0%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    0    0.0%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
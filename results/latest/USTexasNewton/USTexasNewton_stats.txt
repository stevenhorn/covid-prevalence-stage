40 Divergences 
Computed from 160 by 123 log-likelihood matrix

         Estimate       SE
elpd_loo   916.02   306.96
p_loo      530.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       48   39.0%
 (0.5, 0.7]   (ok)          0    0.0%
   (0.7, 1]   (bad)        17   13.8%
   (1, Inf)   (very bad)   58   47.2%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
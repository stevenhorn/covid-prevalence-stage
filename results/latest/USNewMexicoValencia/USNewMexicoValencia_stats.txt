0 Divergences 
Computed from 120 by 121 log-likelihood matrix

         Estimate       SE
elpd_loo  -222.73    10.74
p_loo       16.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      104   86.0%
 (0.5, 0.7]   (ok)         10    8.3%
   (0.7, 1]   (bad)         5    4.1%
   (1, Inf)   (very bad)    2    1.7%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
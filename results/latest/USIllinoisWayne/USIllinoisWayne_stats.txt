50 Divergences 
Computed from 160 by 124 log-likelihood matrix

         Estimate       SE
elpd_loo   103.02   311.75
p_loo      841.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      106   85.5%
 (0.5, 0.7]   (ok)          1    0.8%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)   17   13.7%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
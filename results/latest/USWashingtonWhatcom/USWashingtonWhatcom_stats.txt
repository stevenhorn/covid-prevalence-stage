29 Divergences 
Computed from 120 by 121 log-likelihood matrix

         Estimate       SE
elpd_loo  -368.90    13.48
p_loo       24.52        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      103   85.1%
 (0.5, 0.7]   (ok)         12    9.9%
   (0.7, 1]   (bad)         3    2.5%
   (1, Inf)   (very bad)    3    2.5%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
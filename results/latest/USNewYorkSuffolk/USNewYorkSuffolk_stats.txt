30 Divergences 
Computed from 120 by 121 log-likelihood matrix

         Estimate       SE
elpd_loo  -724.92    18.43
p_loo       35.31        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      100   82.6%
 (0.5, 0.7]   (ok)          6    5.0%
   (0.7, 1]   (bad)         1    0.8%
   (1, Inf)   (very bad)   14   11.6%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
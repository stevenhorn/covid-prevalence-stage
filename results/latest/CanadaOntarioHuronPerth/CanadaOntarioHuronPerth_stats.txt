61 Divergences 
Computed from 600 by 121 log-likelihood matrix

         Estimate       SE
elpd_loo  -108.07    20.07
p_loo       24.15        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      118   97.5%
 (0.5, 0.7]   (ok)          2    1.7%
   (0.7, 1]   (bad)         1    0.8%
   (1, Inf)   (very bad)    0    0.0%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
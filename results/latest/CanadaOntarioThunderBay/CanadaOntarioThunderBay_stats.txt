116 Divergences 
Computed from 600 by 121 log-likelihood matrix

         Estimate       SE
elpd_loo  -158.26    17.63
p_loo       24.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       63   52.1%
 (0.5, 0.7]   (ok)         44   36.4%
   (0.7, 1]   (bad)        13   10.7%
   (1, Inf)   (very bad)    1    0.8%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.
18 Divergences 
Computed from 160 by 124 log-likelihood matrix

         Estimate       SE
elpd_loo  -300.17    19.21
p_loo       18.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      107   86.3%
 (0.5, 0.7]   (ok)         12    9.7%
   (0.7, 1]   (bad)         4    3.2%
   (1, Inf)   (very bad)    1    0.8%


The scale is now log by default. Use 'scale' argument or 'stats.ic_scale' rcParam if
you rely on a specific value.
A higher log-score (or a lower deviance) indicates a model with better predictive
accuracy.